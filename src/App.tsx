import React from "react";

import ComponentX from "ComponentX/container.componentX";

const App: React.FC = () => {
  return (
    <>
      <ComponentX />
    </>
  );
};

export default App;
