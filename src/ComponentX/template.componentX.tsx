/**
 * This file only should contains HTML
 *
 * */

import React from "react";
import "./style.css";

interface IProps {
  title: string;
  onChangeTitle: () => void;
}

export default (props: IProps) => {
  const { title, onChangeTitle } = props;
  return (
    <div className="container">
      <span>{title}</span>
      <button onClick={onChangeTitle}>Change Title Dynamicly</button>
    </div>
  );
};
