/**
 * This file only should contains component logic
 *
 * */

import { useState } from "react";
import template from "./template.componentX";
import { randomValue } from "./logical.componentX";

const Component: React.FC = () => {
  const [index, setIndex] = useState(0);

  const titles: string[] = ["Hello World", "Hola Mundo"];
  const onChangeTitle = () => {
    setIndex(randomValue());
  };
  return template({ title: titles[index], onChangeTitle });
};

export default Component;
