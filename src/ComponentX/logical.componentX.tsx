/**
 * This file only should contains business logic
 *
 * */

const randomValue = () => {
  const value = Math.random();
  return value < 0.5 ? 0 : 1;
};

export { randomValue };
